from jinja2 import Template
import os

template = """hostname {{ hostname }}"""

# Comment
data = {
    "hostname": os.getenv("host", default=None),
}

j2_template = Template(template)

print(j2_template.render(data))
